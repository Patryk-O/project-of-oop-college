﻿using Microsoft.VisualBasic.CompilerServices;
using Object_app;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Object_app
{

    class Student
    {
        public int Index { get; set; }
        private string Name { get; set; }
        private string Lastname { get; set; }
        private string Kierunek { get; set; }
        private Rocznik Rocznik { get; set; }
        private string Email { get; set; }
        private string Telefon { get; set; }

        private List<Przedmioty> przedmioties = new List<Przedmioty>();

        private Dictionary<Przedmioty,List<int>> Oceny = new Dictionary<Przedmioty,List<int>>();


        public Student(int index,string name, string lastname, Rocznik rocznik, string email, string telefon)
        {
            Index = index;
            Name = name;
            Lastname = lastname;
            Kierunek = rocznik.GetKierunek();
            Rocznik = rocznik;
            Email = email;
            Telefon = telefon;
        }

        public string GetName() => Name;

        public string GetLastname() => Lastname;

        public string GetKierunek() => Kierunek;

        public string GetEtapStudiow() => Rocznik.GetEtapStudiow();

        public string Getemail() => Email;

        public string Gettelefon() => Telefon;

        public void AddSubject(Przedmioty przedmioty) => przedmioties.Add(przedmioty);

        public List<Przedmioty> GetPrzedmioties() => przedmioties;

        public void AddJOcene(Przedmioty przedmioty, int ocena)
        {
            if (Oceny.ContainsKey(przedmioty))
            {
                Oceny[przedmioty].Add(ocena);
            }
            else
            {
                List<int> temp = new List<int> { ocena };
                Oceny.Add(przedmioty, temp);
            }
        }
        public Dictionary<Przedmioty, List<int>> WyswietlWszystkieOceny() => Oceny;

        public void GetAverage()
        {
            foreach(var n in Oceny)
            {
                Console.WriteLine($"{n.Key} {n.Value.Average()}");
            }
        }
        
    }
}
