﻿
using Object_app;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Object_app
{
    class Dziekanat
    {
        public static void IloscWszystkichStudentow()
        {
            int count = Program.Students.Count();
            Console.WriteLine( count);
        }
         public static void IloscStudentowNaKierunku()
         {
             var GrupyKierunkow = (Program.Students.GroupBy(i => i.GetKierunek())
                 .Select(i => new
                 {
                     Kierunek = i.Key,
                     LiczbaStudentow = i.Select(x => x).Count()
                 })).ToList();

             foreach(var n in GrupyKierunkow)
             {
                 Console.WriteLine(n);

             }

         }
        public static void IloscStudnetowNaKierunkuIRoku()
        {
            var GrupyKierunkow = from student in Program.Students
                                 where student.GetEtapStudiow() == "1"
                                 group student by student.GetKierunek() into g
                                 select new { Kierunek = g.Key, LiczbaStudentow = g.Count() };



            foreach (var n in GrupyKierunkow)
            {
                Console.WriteLine(n);

            }

        }
        public static void KtoMaNajlepszaOceneZPrzedmiotu(Przedmioty przedmioty)
        {
            Dictionary<int, double> aveg = new Dictionary<int, double>();
            foreach (var n in Program.Students)
            {
                foreach(var i in n.WyswietlWszystkieOceny().Where(x => x.Key == przedmioty))
                {
                    aveg.Add(n.Index, i.Value.Average() );
                }

            }
            double best = aveg.Values.Max();
            Console.WriteLine($"Id Studenta = {aveg.FirstOrDefault(x => x.Value == best).Key} {przedmioty} Srednia = {best}");



        }
        public static void JakaJestSredniaOcenaZPrzedmiotu(Przedmioty przedmiot)
        {
            Dictionary<int, double> aveg = new Dictionary<int, double>();
            foreach (var n in Program.Students)
            {
                foreach (var i in n.WyswietlWszystkieOceny().Where(x => x.Key == przedmiot))
                {
                    aveg.Add(n.Index, i.Value.Average());
                }

            }
            double averge = aveg.Values.Average();
            Console.WriteLine($"{przedmiot} {averge}");

        }

        public static void WszystkieKierunki()
        {
            foreach(string n in Enum.GetNames(typeof(Kierunek)))
            {
                Console.WriteLine(n);
            }
            
        }
        public static void IloscWykladowcow()
        {
            int count = Program.wykladowcas.Count();
            Console.WriteLine("Wykladowcow jest aktualnie: "+count);
        }
        public static void KtoProwadziPrzedmiot()
        {
            var KtoProwadzi = from wykladowca in Program.wykladowcas
                              select new
                              {
                                  Wykladowca = $"{wykladowca.GetName()} {wykladowca.GetLastName()} ",
                                  Przedmiot = wykladowca.GetPrzedmiot()
                              };
            foreach (var n in KtoProwadzi)
            {
                Console.WriteLine(n);
            }

        }
        public static void IloscRocznikow()
        {
            var IleRocznikow = from rocznik in Program.roczniks
                               group rocznik by rocznik.GetRokNaboru() into g
                               select new { Rocznik = $"{g.Key}/{Int32.Parse(g.Key) + 1}" };



            foreach (var n in IleRocznikow)
            {
                Console.WriteLine(n);

            }
        }
        
        public static void DodajPrzedmiotDoKierunku(Kierunek kierunek, Przedmioty przedmioty)
        {
            Program.Students.FindAll(x => x.GetKierunek() == kierunek.ToString()).ForEach(x => x.AddSubject(przedmioty));
        }

        public static void DodajStudenta(int index, string name, string lastname, Rocznik rocznik, string email, string telefon)
        {
            Program.Students.Add(new Student(index, name, lastname, rocznik, email, telefon));
        }
        public static void DodajWykladowce(string name, string lastname, Przedmioty przedmiot, string email, string telefon)
        {
            Program.wykladowcas.Add(new Wykladowca( name,  lastname,  przedmiot,  email,  telefon));
        }
        public static void DodajRocznik(int nrgr, int etapStudiow, int rokNaboru, Kierunek kierunek)
        {
            Program.roczniks.Add(new Rocznik( nrgr,  etapStudiow,  rokNaboru,  kierunek));
        }
    }
}
