﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Object_app
{
    class Wykladowca
    {
        private string Name { get; set; }
        private string Lastname { get; set; }
        private Przedmioty Przedmiot { get; set; }
        private string Email { get; set; }
        private string Telefon { get; set; }


        public Wykladowca(string name, string lastname, Przedmioty przedmiot, string email, string telefon)
        {
            Name = name;
            Lastname = lastname;
            Przedmiot = przedmiot;
            Email = email;
            Telefon = telefon;
        }

        public string GetName() => Name;

        public string GetLastName() => Lastname;

        public Przedmioty GetPrzedmiot() => Przedmiot;

        public string GetEmail() => Email;

        public string GetTelefon() => Telefon;

        
    }
}
