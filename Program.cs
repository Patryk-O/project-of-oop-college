﻿using Microsoft.VisualBasic;
using Object_app;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography.X509Certificates;

namespace Object_app
{
    class Program
    {
        public static List<string> ListaKomend = new List<string>()
        {
           "Ilosc Wszystkich Studentow",
           "Ilosc Studentow Na Kierunkach ",
           "Ilosc Studentow na kierunkach na 1 roku",
           "Kto Ma najlepsze Oceny z Przedmiotu (potrzebny przedmiot)",
           "Jaka jest srednia ocena z przedmiotu (potrzebny przedmiot)",
           "Wszystkie Kierunki",
           "Ilosc Wykladowcow",
           "Jakie Przedmioty prowadza wykladowcy",
           "Ilosc rocznikow",
           "Dodaj Przedmiot do danego kierunku (potrzebne kierunek, przedmiot)",
           "Dodaj studenta (potrzebne index, imie, nazwisko, rocznik, email, telefon)",
           "Dodaj wykladowce (potrzebne imie, nazwisko, przedmiot, email, telefon)",
           "Dodaj Rocznik (potrzebne nr grupy, rok studiow, rok naboru, kierunek)",

        };
        
        //Utworzona statyczna lista roczników
        public static List<Rocznik> roczniks = new List<Rocznik>
        {
            new Rocznik(1,1,2019,Kierunek.Informatyka),
            new Rocznik(2,1,2019,Kierunek.Informatyka),
            new Rocznik(1,1,2019,Kierunek.Administracja),
            new Rocznik(1,3,2016,Kierunek.Pedagogika),
            new Rocznik(3,3,2016,Kierunek.Dziennikarstwo),

        };
        //Utworzona statyczna lista studentów
        public static List<Student> Students = new List<Student>
            {

            new Student(0,"Michal","Andrzej",roczniks[0],"asdfg@wp.pl","879132456"),
            new Student(1,"Andrzej","Aniol",roczniks[0],"faq-cg@wp.pl","264532456"),
            new Student(2,"Ania","Andrzejak",roczniks[1],"trsdfg@wp.pl","879136426"),
            new Student(3,"Sara","Sora",roczniks[4],"saras@gmail.com","175132456"),
            new Student(4,"Kamil","Socha",roczniks[2],"SochaK@gmail.com","782349862"),
            };

        //Utworzona statyczna lista Wykladowców
        public static List<Wykladowca> wykladowcas = new List<Wykladowca>
            {
            new Wykladowca("Andrzej", "Wachowski", Przedmioty.Matematyka, "A_wacho@gmail.com", "684279149"),
            new Wykladowca("Andrzej", "Wozniak", Przedmioty.Fizyka, "Andrzej_wo@gmail.com", "684785149"),
            new Wykladowca("Helena", "Zimowska", Przedmioty.BazyDanych, "Helen-Zimowska@gmail.com", "684279789"),
            new Wykladowca("Alina", "Majowska", Przedmioty.PodstawyProgramowania, "A_Majowska@gmail.com", "614679149"),
            new Wykladowca("Elzbieta", "Ziko", Przedmioty.Algorytmy, "Ziko-Elz@gmail.com", "714654123"),
            };

        static void Main(string[] args)
        {


            //Dodanie przedmiotów do studentów o danym kierunku
            Dziekanat.DodajPrzedmiotDoKierunku(Kierunek.Informatyka, Przedmioty.Matematyka);
            Dziekanat.DodajPrzedmiotDoKierunku(Kierunek.Informatyka, Przedmioty.BazyDanych);
            Dziekanat.DodajPrzedmiotDoKierunku(Kierunek.Informatyka, Przedmioty.Fizyka);
            Dziekanat.DodajPrzedmiotDoKierunku(Kierunek.Informatyka, Przedmioty.PodstawyProgramowania);
            Dziekanat.DodajPrzedmiotDoKierunku(Kierunek.Administracja, Przedmioty.Matematyka);
            Dziekanat.DodajPrzedmiotDoKierunku(Kierunek.Informatyka, Przedmioty.Fizyka);

            //Wypełnienie przedmiotów randomowymi ocenami
            foreach (var n in Students)
            {
                foreach(var p in n.GetPrzedmioties())
                {
                    for(int i=0; i<5;i++) 
                    {
                        var rand = new Random();
                        int ocena = rand.Next(1, 6);
                        n.AddJOcene(p, ocena);
                    }
                    
                }
            }

            //Wyświetla oceny wszystkich uczniów z wszystkich przedmiotów
            //foreach (var n in Students)
            //{
            //    foreach(var o in n.WyswietlWszystkieOceny())
            //    {
            //        Console.Write($"{n.Index} {o.Key}");
            //        foreach (var i in o.Value)
            //        {
            //            Console.Write(i);
            //        }
            //        Console.WriteLine();
            //    }

            //}
            
               

            try
            {
                Console.WriteLine("--help: dostepne komendy");
                bool running = true;
                do {

                    Console.WriteLine("Wspisz nr funkcji ktory ciebie interesuje");
                    int command;
                    string commandstr = Console.ReadLine();
                    if(commandstr == "--help")
                    {
                        for(int i = 0; i < ListaKomend.Count; i++)
                        {
                            Console.WriteLine($"nr komendy {i+1}: {ListaKomend[i]} ");
                        }
                    }
                    else
                    {
                        Int32.TryParse(commandstr, out command);
                        switch (command)
                        {
                            case 0: Console.WriteLine("Czy chcesz wyjsc? y|n");
                                char c = Console.ReadKey().KeyChar;
                                if (c == 'y') running = false;                               
                                break;
                            case 1: Dziekanat.IloscWszystkichStudentow(); break;
                            case 2: Dziekanat.IloscStudentowNaKierunku(); break;
                            case 3: Dziekanat.IloscStudnetowNaKierunkuIRoku(); break;
                            case 4:
                                Console.WriteLine("Podaj Przedmiot dla którego chcesz sprawdzic kto ma najlepsze oceny");
                                string x1 = Console.ReadLine();
                                Enum.TryParse(x1,out Przedmioty przedmiotv1);
                                Dziekanat.KtoMaNajlepszaOceneZPrzedmiotu(przedmiotv1);
                                break;
                            case 5:
                                Console.WriteLine("Podaj Przedmiot dla ktorego chcesz sprawdzic srednia");
                                string x2 = Console.ReadLine();
                                Enum.TryParse(x2, out Przedmioty przedmiotv2);
                                Dziekanat.JakaJestSredniaOcenaZPrzedmiotu(przedmiotv2);
                                break;
                            case 6: Dziekanat.WszystkieKierunki(); break;
                            case 7: Dziekanat.IloscWykladowcow();break;
                            case 8: Dziekanat.KtoProwadziPrzedmiot();break;
                            case 9: Dziekanat.IloscRocznikow();break;
                            case 10:
                                Console.WriteLine("Podaj Kierunek dla którego chcesz dodac przedmiot");
                                string x3 = Console.ReadLine();
                                Console.WriteLine("Podaj Przedmiot");
                                string x4 = Console.ReadLine();
                                Enum.TryParse(x3, out Kierunek kierunek);
                                Enum.TryParse(x3, out Przedmioty przedmiotv3);
                                Dziekanat.DodajPrzedmiotDoKierunku(kierunek, przedmiotv3);
                                break;
                            case 11:
                                int index, idrocznik;
                                string Stuimie, Stunazwisko, Stuemail, Stutelefon;                         
                                Console.WriteLine("Dodajesz nowego studenta przygotuj nastepujace rzeczy: Index, imie, nazwisko, IDrocznik, email, telefon");
                                Console.WriteLine("Podaj Index");
                                Int32.TryParse(Console.ReadLine(), out index);
                                Console.WriteLine("Podaj imie");
                                Stuimie = Console.ReadLine();
                                Console.WriteLine("Podaj Nazwisko");
                                Stunazwisko = Console.ReadLine();
                                Console.WriteLine("Podaj IDrocznika");
                                Int32.TryParse(Console.ReadLine(), out idrocznik);
                                Console.WriteLine("Podaj email");
                                Stuemail = Console.ReadLine();
                                Console.WriteLine("Podaj telefon");
                                Stutelefon = Console.ReadLine();
                                try
                                {
                                    Dziekanat.DodajStudenta(index, Stuimie, Stunazwisko, roczniks[idrocznik], Stuemail, Stutelefon);
                                }
                                catch(Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                                break;
                            case 12:
                                string Wykimie, Wyknazwisko, Wykemail, Wyktelefon;
                                Console.WriteLine("Dodajesz nowego Wykladowce przygotuj nastepujace rzeczy: imie, nazwisko, Przedmiot, email, telefon");
                                Console.WriteLine("Podaj imie");
                                Wykimie = Console.ReadLine();
                                Console.WriteLine("Podaj Nazwisko");
                                Wyknazwisko = Console.ReadLine();
                                Console.WriteLine("Podaj Przedmiot");
                                Enum.TryParse(Console.ReadLine(), out Przedmioty przedmiotyv4);
                                Console.WriteLine("Podaj email");
                                Wykemail = Console.ReadLine();
                                Console.WriteLine("Podaj telefon");
                                Wyktelefon = Console.ReadLine();
                                try
                                {
                                    Dziekanat.DodajWykladowce(Wykimie, Wyknazwisko, przedmiotyv4, Wykemail, Wyktelefon);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                                break;
                            case 13:
                                int nrgr, etapStudiow, rokNaboru;
                                Console.WriteLine("Dodajesz nowey Rocznik przygotuj nastepujace rzeczy: Nr grupy, etap studiow, rok naboru, kierunek");
                                Console.WriteLine("Podaj nr grupy");
                                Int32.TryParse(Console.ReadLine(),out nrgr);
                                Console.WriteLine("Podaj Etap studiow");
                                Int32.TryParse(Console.ReadLine(), out etapStudiow);
                                Console.WriteLine("Podaj rok naboru");
                                Int32.TryParse(Console.ReadLine(), out rokNaboru);
                                Console.WriteLine("Podaj kierunek");
                                Enum.TryParse(Console.ReadLine(), out Kierunek kierunekv2);

                                try
                                {
                                    Dziekanat.DodajRocznik(nrgr, etapStudiow, rokNaboru, kierunekv2);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                                break;

                        }
                    }
                  
                } while (running) ;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }






        }
    }
}
