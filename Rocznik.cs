﻿using Object_app;
using System;
using System.Collections.Generic;

namespace Object_app
{

    public class Rocznik
    {    

        private string NrGr { get; set; }
        private string EtapStudiow { get; set; }
        private string RokNaboru { get; set; }
        private Kierunek Kierunek { get; set; }

        public Rocznik(int nrgr, int etapStudiow,int rokNaboru, Kierunek kierunek)
        {
            NrGr = nrgr.ToString();
            EtapStudiow = etapStudiow.ToString();
            RokNaboru = rokNaboru.ToString();
            Kierunek = kierunek;
        }

        public string GetRocznik()
        {
            return $"{EtapStudiow}/{NrGr}/{Kierunek}/{RokNaboru}";
        }

        public string GetNrGrupy() => NrGr;

        public string GetKierunek() => Kierunek.ToString();

        public string GetRokNaboru() => RokNaboru;

        public string GetEtapStudiow() => EtapStudiow;

        
    }

}
